#Connect-AzAccount
Function variables{

[CmdletBinding()]Param (
[Parameter(Mandatory)]

[string]$subscriptionname

)

$admsg="az-adm-group-" + $subscriptionname
$rdmsg="az-rdm-group-" + $subscriptionname
$bilsg="az-bil-group-" + $subscriptionname
$sgs=$admsg,$rdmsg,$bilsg
$sub=Get-AzSubscription  -SubscriptionName $subscriptionname
$subid=$sub.Id
$sub=Select-AzSubscription -SubscriptionId $sub.Id
$dsccoename=""


}


Function Unassignment-Role {

[CmdletBinding()]Param (
[Parameter(Mandatory)]

[string]$subscriptionname

)

Foreach ($sg in $sgs){
$RoleAssignments=Get-AzRoleAssignment -Scope /subscriptions/$subid | Where-Object {$_.Scope -eq "/subscriptions/" + $subid}
    Foreach ($RoleAssignment in $RoleAssignments){
       
        if ($RoleAssignment.Scope -eq "/subscriptions/" + $subid){
        Remove-AzRoleAssignment  -ObjectId $RoleAssignment.ObjectId -RoleDefinitionName $RoleAssignment.RoleDefinitionName
        }
       


    }
}
}

##función que elimina los grupos de seguridad necesarios acorde al modelo RBAC
Function Delete-SecurityGroups {

[CmdletBinding()]Param (
[Parameter(Mandatory)]

[string]$subscriptionname

)

Foreach ($sg in $sgs){
$GroupIds=Get-AzADGroup -DisplayName  $sg
    Foreach ($GroupId in $GroupIds.Id) {
    Remove-AzADGroup -ObjectId $GroupId -Force

    }
}

}

##función que elimina los diagnostic settings en la suscripción

function DiagnosticSettingCcoeDeleted {

[CmdletBinding()]Param(

[Parameter(Mandatory)]

[string]$subscriptionname

)

Remove-AzDiagnosticSetting -ResourceId /subscriptions/$subid -Name $dsccoename

}


##función que elimina las politicas asociadas a la suscripción y grupos de recursos de forma directa

function RemovePolicy {

[CmdletBinding()]Param(

[Parameter(Mandatory)]

[string]$subscriptionname

)



$AzPolicyAssignments=Get-AzPolicyAssignment -Scope /subscriptions/$subid

foreach ($AzPolicyAssignment in $AzPolicyAssignments){


Remove-AzPolicyAssignment -Scope /subscriptions/$subid -Name $AzPolicyAssignment.Name  -Confirm:$false


}

$rgs=Get-AzResourceGroup

Foreach ($rg  in $rgs){

$AzPolicyAssignmentsrgs=Get-AzPolicyAssignment -Scope $rg.ResourceId

    foreach ($AzPolicyAssignmentsrg in $AzPolicyAssignmentsrgs){
    Remove-AzPolicyAssignment -Scope $rg.ResourceId  -Name $AzPolicyAssignmentsrg.Name  -Confirm:$false
}
}
}


##función que quita la suscripción del MG que lo contiene y lo deja en Tenant Root Group (ubicación por defecto) 


function ManagementGroupReAssignment {



[CmdletBinding()]Param(

[Parameter(Mandatory)]

[string]$subscriptionname

)


$mgs=Get-AzManagementGroup

foreach ($mg in $mgs){

$mgdetails=Get-AzManagementGroup -GroupName $mg.Name -Expand -Recurse
$childsubs=$mgdetails.Children

foreach ($childsub  in $childsubs){


if ($childsub.DisplayName -eq $sub.Name) {

Remove-AzManagementGroupSubscription -GroupName $mg.Name -SubscriptionId $subid


}

}

}

}



variables  -subscriptionname transit-live-xg-test
RemovePolicy -subscriptionname transit-live-xg-test
Unassignment-Role -subscriptionname transit-live-xg-test
Delete-SecurityGroups -subscriptionname transit-live-xg-test
DiagnosticSettingCcoeDeleted -subscriptionname transit-live-xg-test
ManagementGroupReAssignment -subscriptionname transit-live-xg-test
